CC?=		cc
INSTALL?=	install
GZIP?=		gzip
RM?=		rm
MAN=		${PROG}.1.gz
PROG=		dialog4ports
CFLAGS+=	-Wall -pedantic
LDADD+=		-lncursesw -lm
PREFIX?=	/usr/local
BINDIR=		${PREFIX}/bin
MAN1DIR=	${PREFIX}/man/man1
BSD_INSTALL_PROGRAM?=	install -m 0555
BSD_INSTALL_DATA?=	install -m 0444

SRCS=	dialog4ports.c mixedlist.c
OBJECTS=${SRCS:.c=.o}

.if defined{DIALOGSOURCES}
CFLAGS+=	-I${DIALOGSOURCES} -D_XOPEN_SOURCE_EXTENDED
.PATH:	${DIALOGSOURCES}
SRCS+=	arrows.c buttons.c dlg_keys.c help.c inputstr.c mouse.c mousewget.c \
	textbox.c rc.c trace.c ui_getc.c util.c version.c
.else
LDADD+=	-ldialog
.endif

all:	build

build:	${PROG} ${MAN}

install:	${PROG} ${MAN}
	${BSD_INSTALL_PROGRAM} ${PROG} ${DESTDIR}${BINDIR}
	${BSD_INSTALL_DATA} ${MAN} ${DESTDIR}${MAN1DIR}

${PROG}:	${OBJECTS}
	${CC} ${CFLAGS} ${OBJECTS} -o ${PROG} ${LDADD}

${MAN}:	${PROG}.1
	${GZIP} -cn ${PROG}.1 > ${PROG}.1.gz

clean:
	${RM} -f ${PROG} ${OBJECTS} ${PROG}.1.gz
