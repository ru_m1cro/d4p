#ifndef _MIXED_LIST_H
#define _MIXED_LIST_H

#define CTRL_E				5
#define CTRL_R				18
#define F1					265

/* Include all borders, buttons spaces etc.. in dialog window */
#define MAGIC_BORDER		6

typedef struct {
	char *name;
	char *text;
	int type;
	int group;
	bool state;
	bool new;
} dialog_mixedlist;

/* List of items */
#define ITEM_CHECK			1
#define ITEM_RADIO			2
#define ITEM_SEPARATOR		3

int dlg_mixedlist(const char *title, const char *cprompt, int height,
		int min_height, int width, int item_no, dialog_mixedlist *items,
		bool align_center, bool fullscreen);

#endif
