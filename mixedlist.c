/*
 *  Based on checklist.c from dialog
 *
 *  Copyright 2012-2013,2015-2016	Ilya A. Arkhipov
 *  Copyright 2000-2011			Thomas E. Dickey
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License, version 2.1
 *  as published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this program; if not, write to
 *  Free Software Foundation, Inc.
 *  51 Franklin St., Fifth Floor
 *  Boston, MA 02110, USA.
 *
 *  An earlier version of this program lists as authors:
 *  Savio Lam (lam836@cs.cuhk.hk)
 *  Stuart Herbert - S.Herbert@sheffield.ac.uk: radiolist extension
 *  Alessandro Rubini - rubini@ipvvis.unipv.it: merged the two
 */

#include <dialog.h>
#include <dlg_keys.h>
#include <stdbool.h>

#include "mixedlist.h"

static int list_width, check_x, item_x;

#define MIN_HIGH			(1 + (5 * MARGIN))

static int
d4p_default_mixlistitem(dialog_mixedlist *items)
{
	int result = 0;

	if (dialog_vars.default_item != 0) {
		int count = 0;
		while (items->name != 0) {
			if (!strcmp(dialog_vars.default_item, items->name)) {
				result = count;
				break;
			}
			items++;
			count++;
		}
	}
	return result;
}

static int
d4p_calc_mixlist_width(int item_no, dialog_mixedlist *items)
{
	int n, i, len1 = 0, len2 = 0;
	for (i = 0; i < item_no; ++i) {
		if (items[i].type != ITEM_SEPARATOR) {
			if ((n = dlg_count_columns(items[i].name)) > len1)
				len1 = n;
			if ((n = dlg_count_columns(items[i].text)) > len2)
				len2 = n;
		}
	}
	return len1 + len2;
}

static void
d4p_calc_mixlisth(int *height, int *list_height, int item_no)
{
	/* calculate new height and list_height */
	int rows = SLINES - (dialog_vars.begin_set ? dialog_vars.begin_y : 0);
	if (rows - (*height) > 0) {
		if (rows - (*height) > item_no)
			*list_height = item_no;
		else
			*list_height = rows - (*height);
	}
	(*height) = (*list_height) + MAGIC_BORDER; // XXX: add checks for prompt
}

static void
d4p_print_arrows(WINDOW *win,
		int box_x,
		int box_y,
		int scrollamt,
		int choice,
		int item_no,
		int list_height)
{
	dlg_draw_scrollbar(win,
			(long) (scrollamt),
			(long) (scrollamt),
			(long) (scrollamt + choice),
			(long) (item_no),
			box_x + check_x,
			box_x + list_width,
			box_y,
			box_y + list_height + 1,
			menubox_attr,
			menubox_border_attr);
}

 /* Display a help-file as a textbox widget. */
static int
d4p_helpwindow(const char *title,
			const char *file,
			int height,
			int width)
{
	int result = DLG_EXIT_ERROR;

	if (!dialog_vars.in_helpfile && file != 0 && *file != '\0') {
		dialog_vars.in_helpfile = true;
		result = dialog_textbox(title, file, height, width);
		dialog_vars.in_helpfile = false;
	}
	return (result);
}

static void
d4p_clean_window(WINDOW *dialog)
{

	dlg_clear();
	if (dialog != NULL)
		dlg_del_window(dialog);
	dlg_mouse_free_regions();
	refresh();
}


/*
 * Print list item.  The 'selected' parameter is true if 'choice' is the
 * current item.  That one is colored differently from the other items.
 */
static void
print_item(WINDOW *win,
		dialog_mixedlist *items,
		int choice,
		bool selected)
{
	chtype save = dlg_get_attrs(win);
	int i, namelen, slsize;
	chtype attr = A_NORMAL;
	const int *cols;
	const int *indx;
	int limit, itype;
	char const *states;

	itype = items->type;
	/* x for [ ], * for ( ) */
	if (itype == ITEM_CHECK)
		states = " x";
	else if (itype == ITEM_RADIO)
		states = " *";
	else
		states = "  ";

	/* Clear 'residue' of last item */
	wattrset(win, menubox_attr);
	wmove(win, choice, 0);
	for (i = 0; i < list_width; i++)
		waddch(win, ' ');

	/* Is separator? */
	if (itype == ITEM_SEPARATOR) {
		wmove(win, choice, 0);
		if (strlen(items->name) == 0) {
			for (i = 0; i < getmaxx(win); i++)
				waddch(win, ACS_HLINE);
		} else {
			namelen = strlen(items->name) / 2 ;
			slsize = (getmaxx(win) / 2) - namelen - 1;
			if (strlen(items->name) % 2 == 0)
				slsize++;
			for (i = 0; i < slsize ; i++)
				waddch(win, ACS_HLINE);
			waddch(win, ' ');
			wattrset(win, tag_key_attr);
			waddstr(win, items->name );
			wattrset(win, item_attr);
			waddch(win, ' ');
			if (strlen(items->name) % 2 == 0)
				slsize--;
			for (i = 0; i < slsize - 1; i++)
				waddch(win, ACS_HLINE);
		}
	} /* No ;) */
	else {
		wmove(win, choice, check_x - 1);
		wattrset(win, tag_key_attr);
		waddch(win, items->new ? '+' : ' ');
		wattrset(win, selected ? check_selected_attr : check_attr);
		wprintw(win,
				itype == ITEM_CHECK ? "[%c]" : "(%c)",
				states[items->state]);
		wattrset(win, menubox_attr);
		waddch(win, ' ');

		if (strlen(items->name) != 0) {

			indx = dlg_index_wchars(items->name);

			wattrset(win, selected ? tag_key_selected_attr : tag_key_attr);
			waddnstr(win, items->name, indx[1]);

			if ((int) strlen(items->name) > indx[1]) {
				limit = dlg_limit_columns(items->name,
						(item_x - check_x - MAGIC_BORDER), 1);
				if (limit > 1) {
					wattrset(win, selected ? tag_selected_attr : tag_attr);
					waddnstr(win,
							items->name + indx[1],
							indx[limit] - indx[1]);
				}
			}
		}

		if (strlen(items->text) != 0) {
			cols = dlg_index_columns(items->text);
			limit = dlg_limit_columns(items->text,
					(getmaxx(win) - item_x + 1), 0);

			if (limit > 0) {
				wmove(win, choice, item_x);
				wattrset(win, selected ? item_selected_attr : item_attr);
				dlg_print_text(win, items->text, cols[limit], &attr);
			}
		}

		wattrset(win, save);
	}
}

/*
 * This is an alternate interface to 'checklist' which allows the application
 * to read the list item states back directly without putting them in the
 * output buffer.  It also provides for more than two states over which the
 * check/radio box can display.
 */
int
dlg_mixedlist(const char *title,
		const char *cprompt,
		int height,
		int min_height,
		int width,
		int item_no,
		dialog_mixedlist *items,
		bool align_center,
		bool fullscreen)
{
	/* *INDENT-OFF* */
	static DLG_KEYS_BINDING binding[] = {
		ENTERKEY_BINDINGS,
		DLG_KEYS_DATA( DLGK_FIELD_NEXT,	KEY_RIGHT ),
		DLG_KEYS_DATA( DLGK_FIELD_NEXT,	TAB ),
		DLG_KEYS_DATA( DLGK_FIELD_PREV,	KEY_BTAB ),
		DLG_KEYS_DATA( DLGK_FIELD_PREV,	KEY_LEFT ),
		DLG_KEYS_DATA( DLGK_ITEM_FIRST,	KEY_HOME ),
		DLG_KEYS_DATA( DLGK_ITEM_LAST,	KEY_END ),
		DLG_KEYS_DATA( DLGK_ITEM_LAST,	KEY_LL ),
		DLG_KEYS_DATA( DLGK_ITEM_NEXT,	'+' ),
		DLG_KEYS_DATA( DLGK_ITEM_NEXT,	KEY_DOWN ),
		DLG_KEYS_DATA( DLGK_ITEM_NEXT,	CHR_NEXT ),
		DLG_KEYS_DATA( DLGK_ITEM_PREV,	'-' ),
		DLG_KEYS_DATA( DLGK_ITEM_PREV,	KEY_UP ),
		DLG_KEYS_DATA( DLGK_ITEM_PREV,	CHR_PREVIOUS ),
		DLG_KEYS_DATA( DLGK_PAGE_NEXT,	KEY_NPAGE ),
		DLG_KEYS_DATA( DLGK_PAGE_NEXT,	DLGK_MOUSE(KEY_NPAGE) ),
		DLG_KEYS_DATA( DLGK_PAGE_PREV,	KEY_PPAGE ),
		DLG_KEYS_DATA( DLGK_PAGE_PREV,	DLGK_MOUSE(KEY_PPAGE) ),

		END_KEYS_BINDING
	};
	/* *INDENT-ON* */

#ifdef KEY_RESIZE
	int old_height = height;
	int old_width = width;
#endif
	int i, j, key2, x, y, cur_x, cur_y, box_x, box_y;
	int key = 0, fkey;
	int button = dialog_state.visit_items ? -1 : dlg_defaultno_button();
	int choice = d4p_default_mixlistitem(items); // current item
	int scrollamt = 0; // scroll screen
	int max_choice;
	int was_mouse;
	int use_height;
	int use_width, name_width, text_width;
	int result = DLG_EXIT_UNKNOWN;
	WINDOW *dialog = NULL;
	WINDOW *list;
	char *prompt = dlg_strclone(cprompt);
	const char **buttons = dlg_ok_labels();
	bool found;

	dlg_does_output();
	dlg_tab_correct_str(prompt);

	/*
	 * If this is a radiobutton list, ensure that no more than one item is
	 * selected initially.  Allow none to be selected, since some users may
	 * wish to provide this flavor.
	 */
	int group = 0;

	for (i = 0; i < item_no; i++) {
		if (items[i].type == ITEM_RADIO) {
			/* We don't know id radio group */
			if (group == 0 || group < items[i].group)
				group = items[i].group;
			if (items[i].state) {
				if (items[i].group == group) {
					group++;
				} else {
					items[i].state = 0;
				}
			}
		}
	}

	if (min_height != 0 && item_no < min_height)
		height = min_height;


retry:
	d4p_clean_window(dialog);

	if (fullscreen) {
		height = SLINES - 4;
		width = SCOLS - 4;
	}

	use_height = height;
	if (use_height == 0) {
		use_width = d4p_calc_mixlist_width(item_no, items) + 10;
		/* calculate height without items (4) */
		dlg_auto_size(	title, cprompt, &height, &width, MIN_HIGH,
						MAX(26, use_width));
		d4p_calc_mixlisth(&height, &use_height, item_no);
		/* anyway we should have marging */
		if (width >= SCOLS)
			width = SCOLS - 4;
		if (height >= SLINES)
			height = SLINES - 4;
	} else {
		dlg_auto_size(	title, prompt, &height, &width,
						MIN_HIGH + use_height, 26);
	}
	dlg_button_layout(buttons, &width);
	dlg_print_size(height, width);
	dlg_ctl_size(height, width);

	x = dlg_box_x_ordinate(width);
	y = dlg_box_y_ordinate(height);

	dialog = dlg_new_window(height, width, y, x);
	dlg_register_window(dialog, "mixedlist", binding);
	dlg_register_buttons(dialog, "mixedlist", buttons);

	dlg_mouse_setbase(x, y);

	dlg_draw_box(dialog, 0, 0, height, width, dialog_attr, border_attr);
	dlg_draw_bottom_box(dialog);

	dlg_draw_title(dialog, title);

	wattrset(dialog, dialog_attr);
	dlg_print_autowrap(dialog, prompt, height, width);

	list_width = width - MAGIC_BORDER;
	getyx(dialog, cur_y, cur_x);
	box_y = cur_y + 1;
	box_x = (width - list_width) / 2 - 1;

	if (dialog_vars.help_file != NULL) {
		dialog_vars.item_help = true;
		dlg_item_help("Detailed help is available. Press F1 or ^E to view it.");
	}

	/*
	 * After displaying the prompt, we know how much space we really have.
	 * Limit the list to avoid overwriting the ok-button.
	 */
	if (use_height + MIN_HIGH > height - cur_y)
		use_height = height - MIN_HIGH - cur_y;
	if (use_height <= 0)
		use_height = 1;

	max_choice = MIN(use_height, item_no);

	/* create new window for the list */
	list = dlg_sub_window(dialog, use_height, list_width,
			y + box_y + 1, x + box_x + 1);

	/* draw a box around the list items */
	dlg_draw_box(dialog, box_y, box_x,
			use_height + 2 * MARGIN,
			list_width + 2 * MARGIN,
			menubox_border_attr, menubox_attr);

	text_width = 0;
	name_width = 0;
	/* Find length of longest item to center checklist */
	for (i = 0; i < item_no; i++) {
		if (items[i].type != ITEM_SEPARATOR) {
			text_width = MAX(text_width, dlg_count_columns(items[i].text));
			name_width = MAX(name_width, dlg_count_columns(items[i].name));
		}
	}

	/* If the name+text is wider than the list is allowed, then truncate
	 * one or both of them.  If the name is no wider than 1/4 of the list,
	 * leave it intact.
	 */
	use_width = (list_width - MAGIC_BORDER);
	if (text_width + name_width > use_width) {
		int need = (int) (0.25 * use_width);
		if (name_width > need) {
			int want = (int) (use_width * ((double) name_width) /
					(text_width + name_width));
			name_width = (want > need) ? want : need;
		}
		text_width = use_width - name_width;
	}

	if (align_center) {
		check_x = (use_width - (text_width + name_width)) / 2;
	} else {
		check_x = 1;
	}
	item_x = name_width + check_x + MAGIC_BORDER;

	/* ensure we are scrolled to show the current choice */
	if (choice >= (max_choice + scrollamt)) {
		scrollamt = choice - max_choice + 1;
		choice = max_choice - 1;
	}

	/* scrollamt+max_choice should not be more than item number */
	if (scrollamt + max_choice >= item_no)
		scrollamt = item_no - max_choice;

	/* Print the list */
	for (i = 0; i < max_choice; i++) {
		/* Check for first item, and jump to second if 1 is separator */
		if (items[choice + scrollamt].type == ITEM_SEPARATOR)
			choice = 1;
		print_item(list,
				&items[i + scrollamt],
				i, i == choice);
	}
	(void) wnoutrefresh(list);

	/* register the new window, along with its borders */
	dlg_mouse_mkbigregion(box_y + 1, box_x, use_height, list_width + 2,
			KEY_MAX, 1, 1, 1 /* by lines */ );

	d4p_print_arrows(dialog,
			box_x, box_y,
			scrollamt, max_choice, item_no, use_height);

	dlg_draw_buttons(dialog, height - 2, 0, buttons, button, false, width);

	while (result == DLG_EXIT_UNKNOWN) {
		if (button < 0)		/* --visit-items */
			wmove(dialog, box_y + choice + 1, box_x + check_x + 2);

		key = dlg_mouse_wgetch(dialog, &fkey);
		if (dlg_result_key(key, fkey, &result))
			break;

		was_mouse = (fkey && is_DLGK_MOUSE(key));
		if (was_mouse)
			key -= M_EVENT;
		if (was_mouse && (key >= KEY_MAX)) {
			getyx(dialog, cur_y, cur_x);
			i = (key - KEY_MAX);
			if (i < max_choice && items[i + scrollamt].type != ITEM_SEPARATOR ) {
				/* De-highlight current item */
				print_item(list,
						&items[scrollamt + choice],
						choice, false);
				/* Highlight new item */
				choice = (key - KEY_MAX);
				print_item(list,
						&items[scrollamt + choice],
						choice, true);
				(void) wnoutrefresh(list);
				(void) wmove(dialog, cur_y, cur_x);

				key = ' ';	/* force the selected item to toggle */
			} else {
				beep();
				continue;
			}
			fkey = 0;
		} else if (was_mouse && key >= KEY_MIN) {
			key = dlg_lookup_key(dialog, key, &fkey);
		}

		switch (key) {
			case CTRL_R:
				goto retry;
				break;
			case CTRL_E:
			case F1:
				dialog_state.use_shadow = false;
				d4p_helpwindow(" HELP ", dialog_vars.help_file, 0, 0);
				dialog_state.use_shadow = true;
				goto retry;
				break;
		}

		/*
		 * A space toggles the item status.  We handle either a checklist
		 * (any number of items can be selected) or radio list (zero or one
		 * items can be selected).
		 */
		if (key == ' ') {
			int current = scrollamt + choice;
			int next = items[current].state + 1;

			if (next >= 2)
				next = 0;

			getyx(dialog, cur_y, cur_x);
			/* XXX need a case? */
			if (items[current].type == ITEM_CHECK) {	/* checklist? */
				items[current].state = next;
				print_item(list,
						&items[scrollamt + choice],
						choice, true);
			} else if (items[current].type == ITEM_RADIO) {		/* radiolist */
				for (i = 0; i < item_no; i++) {
					if (i != current && items[i].group == items[current].group)
							items[i].state = 0;
				}
				if (items[current].state) {
					items[current].state = 0;
					print_item(list,
							&items[current],
							choice, true);
				} else {
					items[current].state = 1;
					for (i = 0; i < max_choice; i++) {
						print_item(list,
								&items[scrollamt + i],
								i, i == choice);
					}
				}
			} else {
				// Something wrong ;(
				dlg_exiterr("ERROR: Trying push incorrect item!");
			}
			(void) wnoutrefresh(list);
			(void) wmove(dialog, cur_y, cur_x);
			wrefresh(dialog);
			continue;		/* wait for another key press */
		}

		/*
		 * Check if key pressed matches first character of any item tag in
		 * list.  If there is more than one match, we will cycle through
		 * each one as the same key is pressed repeatedly.
		 */
		found = false;
		if (!fkey) {
			if (button < 0 || !dialog_state.visit_items) {
				for (j = scrollamt + choice + 1; j < item_no; j++) {
					if (dlg_match_char(dlg_last_getc(), items[j].name) &&
						items[j].type != ITEM_SEPARATOR ) {
							found = true;
							i = j - scrollamt;
							break;
					}
				}
				if (!found) {
					for (j = 0; j <= scrollamt + choice; j++) {
						if (dlg_match_char(dlg_last_getc(), items[j].name) &&
							items[j].type != ITEM_SEPARATOR) {
								found = true;
								i = j - scrollamt;
								break;
						}
					}
				}
				if (found)
					dlg_flush_getc();
			} else if ((j = dlg_char_to_button(key, buttons)) >= 0) {
				button = j;
				ungetch('\n');
				continue;
			}
		}

		/*
		 * A single digit (1-9) positions the selection to that line in the
		 * current screen.
		 */
		if (!found
				&& (key <= '9')
				&& (key > '0')
				&& (key - '1' < max_choice)) {
			found = true;
			i = key - '1';
		}

		if (!found) {
			if (fkey) {
				found = true;
				switch (key) {
					case DLGK_ITEM_FIRST:
						i = -scrollamt;
						if (items[0].type == ITEM_SEPARATOR)
							i++;
						break;
					case DLGK_ITEM_LAST:
						i = item_no - 1 - scrollamt;
						break;
					case DLGK_PAGE_PREV:
						if (choice) {
							i = 0;
							if (items[scrollamt].type == ITEM_SEPARATOR && scrollamt == 0)
								i++;
							else if (items[scrollamt].type == ITEM_SEPARATOR)
								i--;
						}
						else if (scrollamt != 0) {
							i = -MIN(scrollamt, max_choice);
							if (items[scrollamt + i].type == ITEM_SEPARATOR && (scrollamt + i) == 0)
								i++;
							else if (items[scrollamt + i].type == ITEM_SEPARATOR)
								i--;
							}
						else
							continue;
						break;
					case DLGK_PAGE_NEXT:
						i = MIN(choice + max_choice, item_no - scrollamt - 1);
						if (items[scrollamt + i].type == ITEM_SEPARATOR) {
							if (scrollamt + i + 1 < item_no)
								i++;
							else
								i--;
						}
						break;
					case DLGK_ITEM_PREV:
						i = choice - 1;
						if (items[scrollamt + i].type == ITEM_SEPARATOR && (scrollamt + i) == 0)
							i++;
						else if (items[scrollamt + i].type == ITEM_SEPARATOR)
							i--;
						if (choice == 0 && scrollamt == 0)
							continue;
						break;
					case DLGK_ITEM_NEXT:
						i = choice + 1;
						if (items[scrollamt + i].type == ITEM_SEPARATOR) {
							if (scrollamt + i + 1 < item_no)
								i++;
							else
								i--;
						}
						if (scrollamt + choice >= item_no - 1)
							continue;
						break;
					default:
						found = false;
						break;
				}
			}
		}

		if (found) {
			if (i != choice) {
				getyx(dialog, cur_y, cur_x);
				if (i <= 0 || i >= max_choice) {
					{
						if (i <= 0) {
							if (items[0].type == ITEM_SEPARATOR && (scrollamt + i) == 1) {
								scrollamt += (i - 1);
								choice = 1;
							}
							else {
								scrollamt += i;
								choice = 0;
							}
						} else {
							choice = max_choice - 1;
							scrollamt += (i - max_choice + 1);
						}
						for (i = 0; i < max_choice; i++) {
							print_item(list,
									&items[scrollamt + i],
									i, i == choice);
						}
					}
					(void) wnoutrefresh(list);
					d4p_print_arrows(dialog,
							box_x, box_y,
							scrollamt, max_choice, item_no, use_height);
				} else {
					/* De-highlight current item */
					print_item(list,
							&items[scrollamt + choice],
							choice, false);
					/* Highlight new item */
					choice = i;
					print_item(list,
							&items[scrollamt + choice],
							choice, true);
					(void) wnoutrefresh(list);
					d4p_print_arrows(dialog,
							box_x, box_y,
							scrollamt, max_choice, item_no, use_height);
					(void) wmove(dialog, cur_y, cur_x);
					wrefresh(dialog);
				}
			}
			continue;		/* wait for another key press */
		}

		if (fkey) {
			switch (key) {
				case DLGK_ENTER:
					result = dlg_enter_buttoncode(button);
					break;
				case DLGK_FIELD_PREV:
					button = dlg_prev_button(buttons, button);
					dlg_draw_buttons(dialog, height - 2, 0, buttons, button,
							0, width);
					break;
				case DLGK_FIELD_NEXT:
					button = dlg_next_button(buttons, button);
					dlg_draw_buttons(dialog, height - 2, 0, buttons, button,
							0, width);
					break;
#ifdef KEY_RESIZE
				case KEY_RESIZE:
					/* reset data */
					height = old_height;
					width = old_width;
					/* repaint */
					goto retry;
#endif
				default:
					if (was_mouse) {
						if ((key2 = dlg_ok_buttoncode(key)) >= 0) {
							result = key2;
							break;
						}
						beep();
					}
			}
		} else {
			beep();
		}
	}

	dlg_clear();
	dlg_del_window(dialog);
	dlg_mouse_free_regions();
	free(prompt);
	return result;
}
