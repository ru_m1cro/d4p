# dialog4ports #
dialog4ports is utility for providing a new dialog interface to configure ports options

### Configure ###
dialog4ports can be configure using environment variables:

**D4PHEIGHT** Dialog height. (Default value: 0)

**D4PMINHEIGHT**
Minimal dialog height, for example:
if D4PMINHEIGHT = 5 and count of option elements(1-4) less than D4PMINHEIGHT, then height will extended to 5. (Default value: 0)

**D4PWIDTH**
Dialog width. (Default value: 80)

**D4PFULLSCREEN**
Dialog full screen mode. (Default: N)

**D4PALIGNCENTER**
Text in dialog becomes to center. (Default: N)

**D4PASCIILINES**
Use ASCII character for line drawing. (Default: N)

### AUTHORS:
The dialog4ports utility was written by:

Ilya A. Arkhipov <rum1cro@yandex.ru>
with help from:

Eitan Adler <eadler@FreeBSD.org>,

Baptiste Daroussin <bapt@FreeBSD.org>,

Alexey Dokuchaev <danfe@FreeBSD.org>
